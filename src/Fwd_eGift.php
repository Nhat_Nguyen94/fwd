<?php
namespace Dayone\Issuer;

class Fwd_eGift {

    public function __construct(){

    }

    /**
     * @author Ha Tran <manhhaniit@gmail.com>
     */
    public function view()
    {

        \App::register('Dayone\Issuer\FwdServiceProvider');
        return 'Fwd::fwd_egift';
    }

}